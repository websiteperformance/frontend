FROM harbor.gretzki.ddns.net/docker-cache/nginx:alpine

COPY external/nginx.conf /etc/nginx/conf.d/default.conf
COPY /dist /usr/share/nginx/html

EXPOSE 80
