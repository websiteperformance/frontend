import {Component, OnDestroy, OnInit} from '@angular/core';
import {CommonDataService} from "../../services/CommonDataService";
import {LocalStorageService} from "../../services/local-storage.service";
import {ListTestsService} from "../../services/list-tests.service";
import {Observable, Subscription, timer} from "rxjs";
import {LocalStorageDataset} from "../../services/local-storage-dataset";
import {BaseError} from "../../generic/baseError";
import {ListTestDto} from "../../services/list-test-dto";
import {ListTestEntity} from "../../services/list-test-entity";
import {ActionErrorDialogData} from "../../services/action-error-dialog-data";
import {MessageService} from "primeng/api";
import {ErrorHandlerService} from "../../services/error-handler.service";
import {Either} from "../../generic/Either";
import {PageChangeEventData} from "../../services/pageChangeEventData";
import {MainActionSidebarTo} from "../../components/main-action-sidebar/main-action-sidebar-to";
import {DefaultMainActionSidebarTo} from "../../services/default-main-action-sidebar-to";

@Component({
    selector: 'app-main',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.css'],
    standalone: false
})
export class MainComponent implements OnInit, OnDestroy {
  private readonly _localStorageServiceSubscription!: Subscription;
  private _listTestsSubscription!: Subscription;
  private _timerSubscription!: Subscription;
  private _progressBarSubscription!: Subscription;
  public currentTimeValue: number = 10000;
  public cpt: number = 1;
  public first: number = 0;
  public page: number = 1;
  public pageCount: number = 0;
  public totalDocCount: number = 0;
  public rows: number = 10;
  public autoLoad: boolean = false;
  private _reloadTimer: Observable<number> = timer(0, this.currentTimeValue);
  private _progressBarTimer: Observable<number> = timer(1000, 1000);

  private _localStorageDataSet!: LocalStorageDataset;
  public _mainActionSidebarTo: MainActionSidebarTo = new DefaultMainActionSidebarTo();

  public _testsResults!: ListTestEntity[];
  public userId!: string;
  public sidebarState: boolean = false;

  constructor(
    private commonDataService: CommonDataService,
    private errorHandlerService: ErrorHandlerService,
    private localStorageService: LocalStorageService,
    private listTestsService: ListTestsService,
    private messageService: MessageService
  ) {
    this._localStorageServiceSubscription = this
      .localStorageService
      .getDataAsync()
      .subscribe(dataSet => {
        this._localStorageDataSet = dataSet;
        this.userId = this._localStorageDataSet.userId;
      });
  }

  getCurrentFilterValue(): number {
    return [
      this.calculateValue(this._mainActionSidebarTo.filterNetwork),
      this.calculateValue(this._mainActionSidebarTo.filterPerformanceEntries),
      this.calculateValue(this._mainActionSidebarTo.filterScreenshot),
      this.calculateValue(this._mainActionSidebarTo.filterTiming),
      this.calculateValue(this._mainActionSidebarTo.filterTracing)
    ].reduce((a, c) => a + c);
  }

  calculateValue(value: number): number {
    return value !== -1 ? 1 : 0;
  }

  sidebarStateEventEmitter(sidebarActionTo: MainActionSidebarTo) {
    console.log(JSON.stringify(this._localStorageDataSet));
    this.sidebarState = false;
    if (sidebarActionTo.sidebarState) {
      this._mainActionSidebarTo = sidebarActionTo;
      this.loadData();
    }
  }

  showSidebar(): void {
    this.sidebarState = true;
  }


  ngOnInit(): void {
    this.commonDataService.sendData("Startseite")
    this.handleAutoLoad();
  }

  handleAutoLoad() {
    if (this.autoLoad) {
      this._timerSubscription = this._reloadTimer.subscribe(() => {
        this.loadData();
      });
      this._progressBarSubscription = this._progressBarTimer.subscribe(() => {
        if (this.cpt >= 10)
          this.cpt = 0;
        this.cpt += 1;
      });
    } else {
      if (this._timerSubscription)
        this._timerSubscription.unsubscribe();
      if (this._progressBarSubscription)
        this._progressBarSubscription.unsubscribe();
      this.cpt = 1;
      this.loadData();
    }
  }

  onAutoLoadChange() {
    this.handleAutoLoad();
  }

  onPageChange(event: any) {
    const data = event as PageChangeEventData;
    console.log(JSON.stringify(data));
    this.first = data.first;
    this.pageCount = data.pageCount;
    this.rows = data.rows;
    this.page = data.page;
    this.loadData();
  }

  handleReloadEvent(reload: boolean) {
    console.log("call reload from main.component");
    if (reload) {
      this.loadData();
    }
  }

  loadData() {
    console.log("first: " + this.first);
    console.log("pageCount: " + this.pageCount);
    console.log("totalDocCount: " + this.totalDocCount);
    console.log("rows: " + this.rows);
    console.log("page: " + this.page);
    console.log("filter: " + JSON.stringify(this._mainActionSidebarTo));


    const page = this.first / this.rows + 1;
    console.log("calculated page: " + page);
    const reqPage = page <= 0 ? 1 : page;

    this._listTestsSubscription = this
      .listTestsService
      .getTestsByUserId(this._localStorageDataSet.userId, reqPage, this.rows, this._mainActionSidebarTo)
      .subscribe((resultSet: Either<BaseError, ListTestDto>) => {
        resultSet.match(
          left => {
            const blu: ActionErrorDialogData = {
              headline: "Ein Fehler ist aufgetreten!",
              code: left.errorCode.toString(),
              body: left.errorMessage,
              visibleForSeconds: 3,
              method: left.operation
            }

            this.errorHandlerService.sendData(blu);
          },
          right => {
            if (!right.isError) {
              this._testsResults = right.websiteList;
              this.totalDocCount = right.docCount;
              this.pageCount = right.pageCount;
              this.messageService.add({
                severity: 'success',
                summary: 'Erfolg',
                detail: `${right.websiteList.length} / ${right.docCount} Datensätze wurden erfolgreich geladen`
              })
            }
          });
      });


  }

  ngOnDestroy(): void {
    if (this._localStorageServiceSubscription)
      this._localStorageServiceSubscription.unsubscribe();

    if (this._listTestsSubscription)
      this._listTestsSubscription.unsubscribe();

    if (this._timerSubscription)
      this._timerSubscription.unsubscribe();

    if (this._progressBarSubscription)
      this._progressBarSubscription.unsubscribe();
  }

}
