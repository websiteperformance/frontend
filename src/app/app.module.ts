import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MainComponent} from './main/main/main.component';
import {LoginComponent} from './login/login/login.component';
import {NavigationComponent} from './navigation/navigation/navigation.component';
import {CommonDataService} from "./services/CommonDataService";
import {LocalStorageService} from "./services/local-storage.service";
import { provideHttpClient, withInterceptorsFromDi } from "@angular/common/http";
import {NgOptimizedImage} from "@angular/common";
import {CardModule} from "primeng/card";
import {ButtonModule} from "primeng/button";
import {DividerModule} from "primeng/divider";
import {MenubarModule} from "primeng/menubar";
import {ImageModule} from "primeng/image";
import {ToastModule} from "primeng/toast";
import {MessageService} from "primeng/api";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ActionErrorToastComponent} from "./components/action-error-toast/action-error-toast.component";
import {ErrorHandlerService} from "./services/error-handler.service";
import { TestListElementComponent } from './components/test-list-element/test-list-element.component';
import {ElementActionComponent} from "./components/element-action/element-action.component";
import {ElementStoredDetailsComponent} from "./components/element-stored-details/element-stored-details.component";
import {ProgressBarModule} from "primeng/progressbar";
import {ProgressSpinnerModule} from "primeng/progressspinner";
import {InputSwitchModule} from "primeng/inputswitch";
import {FormsModule} from "@angular/forms";
import {PaginatorModule} from "primeng/paginator";
import {ScrollTopModule} from "primeng/scrolltop";
import {CheckboxModule} from "primeng/checkbox";
import {SelectButtonModule} from "primeng/selectbutton";
import {MainActionSidebarComponent} from "./components/main-action-sidebar/main-action-sidebar.component";
import {BadgeModule} from "primeng/badge";

@NgModule({ declarations: [
        AppComponent,
        MainComponent,
        LoginComponent,
        NavigationComponent,
        ActionErrorToastComponent,
        TestListElementComponent
    ],
    bootstrap: [AppComponent], imports: [BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        NgOptimizedImage,
        CardModule,
        ButtonModule,
        DividerModule,
        MenubarModule,
        ImageModule,
        ToastModule,
        ElementActionComponent,
        ElementStoredDetailsComponent,
        ProgressBarModule,
        ProgressSpinnerModule,
        InputSwitchModule,
        FormsModule,
        PaginatorModule,
        ScrollTopModule,
        CheckboxModule,
        SelectButtonModule,
        MainActionSidebarComponent,
        BadgeModule], providers: [CommonDataService, ErrorHandlerService, LocalStorageService, MessageService, provideHttpClient(withInterceptorsFromDi())] })
export class AppModule {
}
