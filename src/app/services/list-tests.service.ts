import {Injectable} from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {catchError, map, Observable, take} from "rxjs";
import {BaseError} from "../generic/baseError";
import {ListTestDto} from "./list-test-dto";
import {environment} from "../../environments/environment";
import {getErrorHandler} from "../generic/helper";
import {Either} from "../generic/Either";
import {MainActionSidebarTo} from "../components/main-action-sidebar/main-action-sidebar-to";

@Injectable({
  providedIn: 'root'
})
export class ListTestsService {


  getTestsByUserId(userId: string, page: number, elementsPerPage: number, mainActionSidebarTo: MainActionSidebarTo): Observable<Either<BaseError, ListTestDto>> {
    class StorageDataContainer {
      public readonly filterValue: number;
      public readonly filterName: string;

      constructor(filterValue: number, filterName: string) {
        this.filterValue = filterValue;
        this.filterName = filterName;
      }
    }


    const query = [
      new StorageDataContainer(mainActionSidebarTo.filterScreenshot, 'filterScreenshot'),
      new StorageDataContainer(mainActionSidebarTo.filterTiming, 'filterTiming'),
      new StorageDataContainer(mainActionSidebarTo.filterTracing, 'filterTracing'),
      new StorageDataContainer(mainActionSidebarTo.filterNetwork, 'filterNetwork'),
      new StorageDataContainer(mainActionSidebarTo.filterPerformanceEntries, 'filterPerformanceEntries')
    ]
      .map(l => this.calculateFilterValue(l.filterValue, l.filterName))
      .filter(x => x !== null && x.length > 0)
      .join("&");

    return this
      .httpClient
      .get<ListTestDto>(`${environment.apiUrl}api/checks/list/${userId}/${page}/${elementsPerPage}${(query.length > 0) ? `?${query}` : ''}`)
      .pipe(
        take(1),
        map(d => Either.makeRight(d)),
        catchError(getErrorHandler<ListTestDto>('getTestByUserId'))
      )
  }

  calculateFilterValue(origin: number, queryValue: string): string {
    if (origin !== -1 && (origin === 0 || origin === 1))
      return (origin === 1) ? `${queryValue}=true` : `${queryValue}=false`;
    return '';
  }


  constructor(private httpClient: HttpClient) {
  }
}
