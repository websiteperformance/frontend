import { TestBed } from '@angular/core/testing';

import { RemoveTestService } from './remove-test.service';
import { HttpClient } from "@angular/common/http";

describe('RemoveTestService', () => {
  let service: RemoveTestService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['post']);
    service = new RemoveTestService(httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
