export interface ListTestEntity {
  id: string;
  userId: string;
  url: string;
  location: number;
  ctime: Date;
  width: number;
  height: number;
  userAgent: string;
  hasScreenshot: boolean;
  hasTimings: boolean;
  hasPerformanceEntries: boolean;
  hasTracing: boolean;
  hasNetwork: boolean;
  screenshotId: string;
  timingsId: string;
  performanceEntriesId: string;
  tracingId: string;
  networkId: string;
}
