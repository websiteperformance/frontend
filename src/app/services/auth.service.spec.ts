import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { HttpClient } from "@angular/common/http";
import {H} from "@angular/cdk/keycodes";

describe('AuthService', () => {
  let service: AuthService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['post']);
    service = new AuthService(httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
