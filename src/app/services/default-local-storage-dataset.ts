import {LocalStorageDataset} from "./local-storage-dataset";
import {environment} from "../../environments/environment";
import {MainActionSidebarTo} from "../components/main-action-sidebar/main-action-sidebar-to";

export class DefaultLocalStorageDataset implements LocalStorageDataset {
  isAnonymous: boolean = true;
  userId: string = environment.anonymousUserId;
  userName: string = "Anonymous";
}


