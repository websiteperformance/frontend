export interface LoginResponse {
  bearerToken: string;
}
