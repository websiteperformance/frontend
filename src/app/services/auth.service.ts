import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import {catchError, map, Observable, take} from "rxjs";
import {BaseError} from "../generic/baseError";
import {LoginResponse} from "./login-response";
import {LoginDto} from "./login-dto";
import {environment} from "../../environments/environment";
import {getErrorHandler} from "../generic/helper";
import {Either} from "../generic/Either";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
    observe: 'response' as 'response'
  };

  doLogin(userName: string, password: string): Observable<Either<BaseError, LoginResponse>> {
    const loginDto: LoginDto = {
      userName: userName,
      password: password
    };

    return this
      .httpClient
      .post<Response>(`${environment.apiUrl}api/account/login`, loginDto, this.httpOptions)
      .pipe(
        take(1),
        map(f => {
          const responseObj: LoginResponse = {
            bearerToken: f.headers.get('Authorization') ?? ""
          };
          return Either.makeRight(responseObj);
        }),
        catchError(getErrorHandler<LoginResponse>('doLogin'))
      );


  }

  constructor(private httpClient: HttpClient) {
  }
}
