import {MainActionSidebarTo} from "../components/main-action-sidebar/main-action-sidebar-to";

export class DefaultMainActionSidebarTo implements MainActionSidebarTo {
  filterNetwork: number = -1;
  filterTiming: number = -1;
  filterPerformanceEntries: number = -1;
  filterScreenshot: number = -1;
  filterTracing: number = -1;
  sidebarState: boolean = false;
}
