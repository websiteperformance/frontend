export interface ActionErrorDialogData {
  headline: string;
  body: string;
  code: string;
  visibleForSeconds: number;
  method: string;
}
