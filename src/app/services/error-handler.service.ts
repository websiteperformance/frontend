import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";
import {ActionErrorDialogData} from "./action-error-dialog-data";

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService {
  private subject: Subject<ActionErrorDialogData> = new Subject();

  sendData(actionErrorDialogData: ActionErrorDialogData) {
    this.subject.next(actionErrorDialogData);
  }

  getData(): Observable<ActionErrorDialogData> {
    return this.subject.asObservable();
  }

}
