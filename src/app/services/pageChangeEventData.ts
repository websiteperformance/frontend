export interface PageChangeEventData {
  first: number;
  rows: number;
  page: number;
  pageCount: number;
}
