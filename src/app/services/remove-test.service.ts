import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {catchError, map, Observable, take} from "rxjs";
import {BaseError} from "../generic/baseError";
import {Either} from "../generic/Either";
import {environment} from "../../environments/environment";
import {getErrorHandler} from "../generic/helper";

@Injectable({
  providedIn: 'root'
})
export class RemoveTestService {

  removeTestByTestIdAndUserId(testId: string, userId: string): Observable<Either<BaseError, string>> {
    return this
      .httpClient
      .delete<string>(`${environment.apiUrl}api/checks/delete/${userId}/${testId}`)
      .pipe(
        take(1),
        map(d => Either.makeRight(d)),
        catchError(getErrorHandler<string>('removeTestByTestIdAndUserId'))
      );
  }

  constructor(private httpClient: HttpClient) { }
}
