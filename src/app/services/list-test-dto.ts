import {ListTestEntity} from "./list-test-entity";

export interface ListTestDto {
  isError: boolean;
  errorMessage: string;
  pageCount: number;
  docCount: number;
  websiteList: ListTestEntity[];
}
