import {CommonDataService} from "./CommonDataService";
import {of} from "rxjs";

describe('CommonDataService', () => {
  let service: CommonDataService;
  let testee: string = 'abcde';

  beforeEach(() => {
    service = new CommonDataService();
  })

  it('should be created', () => {
    expect(service).toBeTruthy();
  })

  it('should add and store an entry', () => {
    spyOn(service, 'sendData');
    service.sendData(testee);
    expect(service.sendData).toHaveBeenCalled();
  });

  it('should read an entry', () => {

    let response: string = '';
    spyOn(service, 'getData').and.returnValue(of(testee));

    service.getData().subscribe(d => response = d);
    expect(testee).toEqual(response);
  })

  it('should remove an entry', () => {
    let value: string = '';
    service.getData().subscribe(p => value = p);
    service.sendData(testee);
    expect(testee).toEqual(value);
    service.clearData();
    expect(value).toBeNull();

  });

});
