import {MatPaginatorIntl} from "@angular/material/paginator";

export function PaginatorLocalization() {
  const customPaginatorIntl = new MatPaginatorIntl();

  customPaginatorIntl.itemsPerPageLabel = 'Ergebnisse pro Seite:';

  customPaginatorIntl.getRangeLabel = (page, pageSize, length) => {
    let from = (page === 0) ? 1 : (pageSize * page) + 1;

    let toPage = page + 1;
    let to = ((pageSize * toPage) < length) ? (pageSize * toPage) : length;
    return `Entität: ${from} bis ${to} von ${length}`;
  };

  customPaginatorIntl.nextPageLabel = "Nächste Seite";
  customPaginatorIntl.firstPageLabel = "Erste Seite";
  customPaginatorIntl.lastPageLabel = "Letzte Seite";
  customPaginatorIntl.previousPageLabel = "Vorige Seite";

  return customPaginatorIntl;
}
