import { TestBed } from '@angular/core/testing';

import { ListTestsService } from './list-tests.service';
import { HttpClient } from "@angular/common/http";
import {LocalStorageService} from "./local-storage.service";
import {of} from "rxjs";
import {DefaultLocalStorageDataset} from "./default-local-storage-dataset";
import {environment} from "../../environments/environment";
import {DefaultMainActionSidebarTo} from "./default-main-action-sidebar-to";

describe('ListTestsService', () => {
  let service: ListTestsService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  let localStorageServiceClientSpy: jasmine.SpyObj<LocalStorageService>;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    localStorageServiceClientSpy = jasmine.createSpyObj('LocalStorageService', ['getDataAsync']);

    localStorageServiceClientSpy.getDataAsync.and.returnValue(of(new DefaultLocalStorageDataset))
    httpClientSpy.get.and.returnValue(of(''));

  });

  it('should be created', () => {
    service = new ListTestsService(httpClientSpy);
    expect(service).toBeTruthy();
  });

  it('should build a path without queryparam if no filter is selected', () =>{
    const testDataset = new DefaultMainActionSidebarTo();
    const testLocalStorageDataset = new DefaultLocalStorageDataset();
    service = new ListTestsService(httpClientSpy);
    service.getTestsByUserId(testLocalStorageDataset.userId, 1, 10, testDataset).subscribe();
    const expectedUrl = `${environment.apiUrl}api/checks/list/${testLocalStorageDataset.userId}/1/10`;
    expect(httpClientSpy.get.calls.mostRecent().args[0]).toBe(expectedUrl);

  });

  it('should build query string correctly based on LocalStorageDataset', () => {

    const testDataset = new DefaultMainActionSidebarTo();
    const testLocalStorageDataset = new DefaultLocalStorageDataset();

    testDataset.filterNetwork = 1;

    service = new ListTestsService(httpClientSpy);

    service.getTestsByUserId(testLocalStorageDataset.userId, 1, 10, testDataset).subscribe();

    const expectedUrl = `${environment.apiUrl}api/checks/list/${testLocalStorageDataset.userId}/1/10?filterNetwork=true`;
    expect(httpClientSpy.get.calls.mostRecent().args[0]).toBe(expectedUrl);
  });

  it('should build query string correctly within multiple parameters based on LocalStorageDataset', () => {
    const testDataset = new DefaultMainActionSidebarTo();
    const testLocalStorageDataset = new DefaultLocalStorageDataset();

    testDataset.filterNetwork = 1;
    testDataset.filterScreenshot = 0;
    testDataset.filterPerformanceEntries = 1;
    testDataset.filterTiming = 0;

    service = new ListTestsService(httpClientSpy);

   service.getTestsByUserId(testLocalStorageDataset.userId, 1, 10, testDataset).subscribe();

    const expectedUrl = `${environment.apiUrl}api/checks/list/${testLocalStorageDataset.userId}/1/10?filterScreenshot=false&filterTiming=false&filterNetwork=true&filterPerformanceEntries=true`;
    expect(httpClientSpy.get.calls.mostRecent().args[0]).toBe(expectedUrl);
  });

});
