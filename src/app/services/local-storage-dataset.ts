export interface LocalStorageDataset {
  isAnonymous: boolean;
  userId: string;
  userName: string;
}
