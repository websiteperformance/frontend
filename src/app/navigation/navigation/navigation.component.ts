import {Component, OnDestroy, OnInit} from '@angular/core';
import {CommonDataService} from "../../services/CommonDataService";
import {Subscription} from "rxjs";
import {LocalStorageService} from "../../services/local-storage.service";
import {MenuItem} from "primeng/api";

@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.css'],
    standalone: false
})
export class NavigationComponent implements OnInit, OnDestroy {
  subscription!: Subscription;
  items!: MenuItem[];
  title!: string;
  userName!: string

  constructor(private commonDataService: CommonDataService, private localStorageService: LocalStorageService) {

  }

  ngOnDestroy(): void {
    if (this.commonDataService) {
      this.commonDataService.clearData();
      this.subscription.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.subscription = this.commonDataService.getData().subscribe(d => {
      this.title = d;
      this.items = [
        {
          label: 'Home',
          icon: 'pi pi-fw pi-home',
          routerLink: '/home',
          disabled: this.title === 'Startseite'
        },
        {
          label: 'Login',
          icon: 'pi pi-fw pi-lock',
          routerLink: '/login',
          disabled: this.title === 'Login'
        }
      ];
    });
    this.localStorageService.getDataAsync().subscribe(d => this.userName = d.userName);

  }
}
