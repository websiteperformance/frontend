export class Either<T, U> {
  private readonly left?: T;
  private readonly right?: U;

  private constructor(left?: T, right?: U) {
    this.left = left;
    this.right = right;
  }

  static makeLeft<T>(value: T): Either<T, never> {
    return new Either<T, never>(value);
  }

  static makeRight<U>(value: U): Either<never, U> {
    return new Either<never, U>(undefined, value);
  }

  isLeft(): this is Either<T, never> {
    return this.left !== undefined;
  }

  isRight(): this is Either<never, U> {
    return this.right !== undefined;
  }

  match(left: (value: T) => void, right: (value: U) => void): void {
    if (this.isLeft()) {
      left(this.left as T);
    } else if (this.isRight()) {
      right(this.right as U);
    }
  }

  // matchExec<V>(left: (value: T) => V, right: (value: U) => V): V {
  //   if (this.isLeft()) {
  //     return left(this.left as T);
  //   } else if (this.isRight()) {
  //     return right(this.right as U);
  //   }
  //   throw new Error('Neither left nor right is executable!');
  // }
  //
  // unwrap(): T | U {
  //   if (this.isLeft()) {
  //     return this.left as T;
  //   } else if (this.isRight()) {
  //     return this.right as U;
  //   }
  //   throw new Error('Either is not initialized correctly');
  // }
}
