import {Component, OnDestroy, OnInit} from '@angular/core';
import {CommonDataService} from "../../services/CommonDataService";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    standalone: false
})
export class LoginComponent implements OnInit, OnDestroy{

  constructor(private commonDataService: CommonDataService) {
  }

  ngOnInit(): void {
    this.commonDataService.sendData("Login")
  }

  ngOnDestroy(): void {

  }




}
