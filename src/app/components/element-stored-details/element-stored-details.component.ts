import {Component, Input} from '@angular/core';
import {ListTestEntity} from "../../services/list-test-entity";
import {FormsModule} from "@angular/forms";
import {InputSwitchModule} from "primeng/inputswitch";
import {fakeAsync} from "@angular/core/testing";

@Component({
    selector: 'app-element-stored-details',
    imports: [
        FormsModule,
        InputSwitchModule
    ],
    templateUrl: './element-stored-details.component.html',
    styleUrl: './element-stored-details.component.css'
})
export class ElementStoredDetailsComponent {
  @Input() testItem!: ListTestEntity;

  protected readonly fakeAsync = fakeAsync;
}
