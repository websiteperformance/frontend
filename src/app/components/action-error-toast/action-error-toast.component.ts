import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {MessageService} from "primeng/api";
import {ActionErrorDialogData} from "../../services/action-error-dialog-data";
import {ErrorHandlerService} from "../../services/error-handler.service";
import {Subscription} from "rxjs";

@Component({
    selector: 'app-action-error-toast',
    templateUrl: './action-error-toast.component.html',
    styleUrls: ['./action-error-toast.component.css'],
    standalone: false
})
export class ActionErrorToastComponent implements OnInit, OnDestroy {
  subscription!: Subscription

  ngOnInit(): void {
    this.subscription = this.errorHandlerService.getData().subscribe(d => {
      this.showErrorTemplateToast(d);
    });
  }

  ngOnDestroy(): void {
    if (this.subscription)
      this.subscription.unsubscribe();
  }


  showErrorTemplateToast(actionError: ActionErrorDialogData) {
    this.messageService.add({
      key: 'composedError',
      severity: 'error',
      summary: 'Fehler',
      detail: 'Ein Fehler ist aufgetreten. Details:',
      life: actionError.visibleForSeconds * 1000,
      data: actionError
    })
  }

  constructor(private messageService: MessageService, private errorHandlerService: ErrorHandlerService) {
  }


}
