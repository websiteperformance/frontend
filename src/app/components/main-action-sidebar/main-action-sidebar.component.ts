import {Component, EventEmitter, Input, Output} from '@angular/core';
import {SidebarModule} from "primeng/sidebar";
import {SelectButtonModule} from "primeng/selectbutton";
import {FormsModule} from "@angular/forms";
import {ButtonModule} from "primeng/button";
import {MainActionSidebarTo} from "./main-action-sidebar-to";

@Component({
    selector: 'app-main-action-sidebar',
    imports: [
        SidebarModule,
        SelectButtonModule,
        FormsModule,
        ButtonModule
    ],
    templateUrl: './main-action-sidebar.component.html',
    styleUrl: './main-action-sidebar.component.css'
})
export class MainActionSidebarComponent {
  public currentHasScreenshot: number;
  public currentHasTiming: number;
  public currentHasNetwork: number;
  public currentHasPerformanceEntries: number;
  public currentHasTracing: number;
  @Input() mainActionSidebarTo!: MainActionSidebarTo;
  @Input() sidebarVisible: boolean = false;
  @Output() localStorageDatasetEmitter: EventEmitter<MainActionSidebarTo> = new EventEmitter<MainActionSidebarTo>();

  filterOptions: any[] = [
    {name: "Vorhanden", value: 1},
    {name: "Deaktiviert", value: -1},
    {name: "Nicht vorhanden", value: 0}
  ]

  constructor() {
    this.currentHasNetwork = this.mainActionSidebarTo?.filterNetwork ?? -1;
    this.currentHasScreenshot = this.mainActionSidebarTo?.filterScreenshot ?? -1;
    this.currentHasPerformanceEntries = this.mainActionSidebarTo?.filterPerformanceEntries ?? -1;
    this.currentHasTiming = this.mainActionSidebarTo?.filterTiming ?? -1;
    this.currentHasTracing = this.mainActionSidebarTo?.filterTracing ?? -1;
  }

  cancelSidebar() {
    this.sidebarVisible = false;
    this.mainActionSidebarTo.sidebarState = false;
    this.localStorageDatasetEmitter.emit(this.mainActionSidebarTo);
  }

  onCloseSidebar() {
    this.sidebarVisible = false;
    this.mainActionSidebarTo.filterPerformanceEntries = this.currentHasPerformanceEntries;
    this.mainActionSidebarTo.filterTiming = this.currentHasTiming;
    this.mainActionSidebarTo.filterTracing = this.currentHasTracing;
    this.mainActionSidebarTo.filterNetwork = this.currentHasNetwork;
    this.mainActionSidebarTo.filterScreenshot = this.currentHasScreenshot;
    this.mainActionSidebarTo.sidebarState = true;
    this.localStorageDatasetEmitter.emit(this.mainActionSidebarTo);
  }


}
