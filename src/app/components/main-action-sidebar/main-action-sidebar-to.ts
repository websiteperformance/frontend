import {LocalStorageDataset} from "../../services/local-storage-dataset";

export interface MainActionSidebarTo{
  sidebarState: boolean;
  filterScreenshot: number;
  filterNetwork: number;
  filterTracing: number;
  filterTiming: number;
  filterPerformanceEntries: number;
}
