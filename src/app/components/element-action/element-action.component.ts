import {Component, EventEmitter, Input, OnDestroy, Output} from '@angular/core';
import {CardModule} from "primeng/card";
import {ButtonModule} from "primeng/button";
import {RemoveTestService} from "../../services/remove-test.service";
import {Subscription} from "rxjs";
import {Either} from "../../generic/Either";
import {BaseError} from "../../generic/baseError";
import {ActionErrorDialogData} from "../../services/action-error-dialog-data";
import {ErrorHandlerService} from "../../services/error-handler.service";
import {MessageService} from "primeng/api";

@Component({
    selector: 'app-element-action',
    imports: [
        CardModule,
        ButtonModule
    ],
    templateUrl: './element-action.component.html',
    styleUrl: './element-action.component.css'
})
export class ElementActionComponent implements OnDestroy{
  @Input() testItemId!: string;
  @Input() userId!: string;
  @Output() reloadPage: EventEmitter<boolean> = new EventEmitter<boolean>();

  private _removeTestServiceSubscription!: Subscription;

  removeTestForUser(): void {
    console.log("remove entry with id " + this.testItemId + " for user " + this.userId);
    this._removeTestServiceSubscription = this
      .removeTestService
      .removeTestByTestIdAndUserId(this.testItemId, this.userId)
      .subscribe((either: Either<BaseError, string>) => {
        either.match(
          left => {
            const err: ActionErrorDialogData = {
              headline: "Ein Fehler ist aufgetreten!",
              code: left.errorCode.toString(),
              body: left.errorMessage,
              visibleForSeconds: 4,
              method: left.operation
            }
            console.log("running into left either part");
            this.errorHandlerService.sendData(err);
          },
          right => {
            this
              .messageService
              .add({
                severity: 'success',
                summary: 'Meldung',
                detail: right
              });
            console.log("running into right either part");
            this.reloadPage.emit(true);
          }
        )
      })
  }

  ngOnDestroy(): void {
    if(this._removeTestServiceSubscription)
      this._removeTestServiceSubscription.unsubscribe();
  }



  constructor(private removeTestService: RemoveTestService, private errorHandlerService: ErrorHandlerService, private messageService: MessageService) {


  }


}
