import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ListTestEntity} from "../../services/list-test-entity";
import {environment} from "../../../environments/environment";

@Component({
    selector: 'app-test-list-element',
    templateUrl: './test-list-element.component.html',
    styleUrls: ['./test-list-element.component.css'],
    standalone: false
})
export class TestListElementComponent {
  @Input() testItem!: ListTestEntity;
  @Input() userId!: string;
  @Output() reloadPage: EventEmitter<boolean> = new EventEmitter<boolean>();

  buildScreenshotUrl(bucketName: string, id: string): string {
    return environment
      .screenshotTemplateUrl
      .replace('{bucket}', bucketName)
      .replace('{id}', id);
  }

  setDefaultImage(event: ErrorEvent) {
    const img = event.target as HTMLImageElement;

    img.src = '/assets/ina.png';
  }

  onReloadEventEmit(event: boolean){
    console.log("emit event on test-list-element.component");
    this.reloadPage.emit(event);
  }

  protected readonly localStorage = localStorage;
}
