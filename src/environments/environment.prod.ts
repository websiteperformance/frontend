export const environment = {
  production: true,
  apiUrl: 'http://192.168.0.53:10008/',
  anonymousUserId: '5d5e9a21-270b-466e-b8e7-3c64377f5bb8',
  screenshotTemplateUrl: 'http://192.168.0.53:10006/api/storage/directGet/{bucket}/{id}'
};
